#!/bin/bash

ps -ef | grep yoodle-5.0.0.jar | grep -v grep | awk '{print $2}' | xargs kill

service tomcat stop
